<?php
include("mise_en_page.php");
include("partial/listematchs.php");
include ("partial/prochaine_journee.php");

entete();
menu_nav();
?>

<div class='w-100 bg'>

    <div class="title_accueil main_titre text-center">
        Le petit parieur
    </div>

    <div class="custom-bold title_accueil second_titre text-center">

        Le plus grand terrain de sport !

    </div>

</div>

<div class="div_principale">

    <div class="text_principal">

        Envie de te confronter à tes amis. Viens tester tes compétences et montre à tout le monde que tu es le meilleur !

    </div>

    <?php     if (isset($_SESSION['username'])){}
                else { ?>

                    <div class="bouton_acc">
                        <div>

                            <div class="mt-2 mb-2">
                                Si vous n'êtes pas encore inscrit, c'est le moment !
                            </div>
                            <a class="btn btn-primary" href="registration.php">S'inscrire</a>


                        </div>

                        <div>

                            <div class="mt-2 mb-2">

                                Sinon, connecte toi pour parier !

                            </div>

                            <a class="btn btn-primary" href="login.php">Se connecter</a>


                        </div>


                    </div>

              <?php  }?>


    <div class="accueil_match">

        <div>

            Voici les prochains matchs sur lesquels vous pouvez parier :

        </div>
        <div class="div_tableau">

            <?php formulaire_pari(prochaine_journee())?>


        </div>

    </div>

</div>

<?php
pied();
?>