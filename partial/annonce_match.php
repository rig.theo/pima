<?php
function annonce_match($h_team, $a_team){
    ?>

    <div class="div_match row m-0">

        <?php  $url = $_SERVER['PHP_SELF'];
        $reg = '#^(.+[\\\/])*([^\\\/]+)$#';
        $onestla = preg_replace($reg, '$2', $url);
        ?>
        <?php if ($onestla == "accueil.php") {?>

            <div class="titre_match col-12 text-center"> <?php echo $h_team ?> - <?php echo $a_team ?> </div>
        <?php } else {?>

            <div class="titre_match col-10 text-center"> <?php echo $h_team ?> - <?php echo $a_team ?> </div>

            <div class="choix_resultat col-2 text-center">

                <input type="radio" name="choix<?php echo $h_team ?>" value="HOME_TEAM"/>
                <input type="radio" name="choix<?php echo $h_team ?>" value="DRAW"/>
                <input type="radio" name="choix<?php echo $h_team ?>" value="AWAY_TEAM"/>

            </div>

        <?php } ?>

    </div>

<?php
}
?>