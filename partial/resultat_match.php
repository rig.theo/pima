<?php
function resultat_match($match, $h_team, $a_team){
    ?>

    <div class="div_match row m-0">

        <div class="titre_match col-10 text-center"> <?php echo $h_team ?> - <?php echo $a_team ?> </div>

        <div class="choix_resultat col-2 text-center">

            <?php echo $match->score->fullTime->homeTeam ?> - <?php echo $match->score->fullTime->awayTeam ?>

        </div>


    </div>

<?php
}
?>