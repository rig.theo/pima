<?php
function prochaine_journee(){

        $uri = 'https://api.football-data.org/v2/competitions/FL1';
        $reqPrefs['http']['method'] = 'GET';
        $reqPrefs['http']['header'] = 'X-Auth-Token: 3668a65cbc12427c98a6e4e6f4f41bbf';
        $stream_context = stream_context_create($reqPrefs);
        $response = file_get_contents($uri, false, $stream_context);
        $league = json_decode($response);

        return $league->currentSeason->currentMatchday + 1;
}