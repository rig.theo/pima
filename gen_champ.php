<?php
include("mise_en_page.php");


entete();
menu_nav();
?>

    <div class="container">
        <div class="row">
            <div class="col-10">
                <?php include("championnat_pari.php") ?>
            </div>

            <div class="col-2 justify-content-center">
                <?php include("classement.php") ?>
            </div>
        </div>
    </div>

<?php
pied();
?>