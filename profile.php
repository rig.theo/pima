<?php
include("mise_en_page.php");
include("partial/annonce_match.php");


entete("Accueil");
menu_nav();
?>
<div class="container">
    <div class="row">
        <div class="col-6 border-right-primary justify-content-center position-relative">
            <div class="row justify-content-center mb-3">
                <p class="text-title custom-bold">Mes championnats</p>
            </div>
            <?php 
                $champ = new mysqli("localhost","root","","championnat");
                $user = $_SESSION['username'];
                $tabchamp = $champ->query("SELECT nom_champ FROM participe WHERE nom_user = '$user'");
            ?>
            <div class="row liste_championnats mb-3">
                <div class="col-12">
                    <?php While($nomchamp = $tabchamp->fetch_assoc()){
                        echo "<p class='mb-2'><a href='gen_champ.php?champ=".$nomchamp['nom_champ']."'> ".$nomchamp['nom_champ']." </a></p>";
                    }
                    ?>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-6 text-center position-absolute" style="bottom: 0; left: 0">
                    <form action="new_champ.php" method="post">
                    <input class="btn btn-primary" type="submit" value="Créer un championnat" name="Créer un Championnat"> </form>
                </div>
                <div class="col-6 text-center position-absolute" style="bottom:0; right: 0">
                    <form action="join_champ.php" method="post">
                    <input class="btn btn-primary" type="submit" value="Rejoindre un championnat" name="Rejoindre un championnat"> </form>
                </div>
            </div>
        </div>

        <div class="col-6 justify-content-center">
            <div class="row justify-content-center">
                <p class="text-title custom-bold">Mes informations</p>
            </div>

            <?php 
                $con = new mysqli("localhost","root","","register");
                $result = $con->query("SELECT username, email FROM users WHERE username = '$user'");
                $fetch_data = $result->fetch_assoc();
            ?>

            <div class="row">
                <div class="col-7 pl-4 justify-content-center">
                    <span class="custom-bold">Pseudo :</span>
                    <span><?php echo $fetch_data['username'] ?></span>
                </div>
                <div class="col-7 pl-4 justify-content-center">
                    <span class="custom-bold">Email :</span>
                    <span> <?php echo $fetch_data['email'] ?> </span>
                </div>
                <!-- <div class="col-12 text-center">
                     <form action="changer_info.php" method="post">
                    <input class="btn btn-primary" type="submit" value="Modifier mes informations" name="Modifier mes informations"> </form>
                    
                </div> -->
            </div>
        </div>
    </div>
</div>

<?php
pied();
?>